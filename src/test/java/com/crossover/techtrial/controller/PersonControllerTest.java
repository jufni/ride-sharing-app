/**
 *
 */
package com.crossover.techtrial.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.repositories.PersonRepository;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @author kshah
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PersonControllerTest {

    MockMvc mockMvc;

    @Mock
    private PersonController personController;

    @Autowired
    private TestRestTemplate template;

    @Autowired
    PersonRepository personRepository;

    private Long testId;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(personController).build();
        Person person = new Person();
        person.setName("Jriz");
        person.setRegistrationNumber("4DTC");
        person.setEmail("jriz@gmail.com");
        personRepository.save(person);
        testId = person.getId();
    }

    @After
    public void clean() {
    	personRepository.deleteById(testId);
    }
    
    @Test
    public void contextLoads() {
        assertThat(personController).isNotNull();
    }

    @Test
    public void testPanelShouldBeRegistered() {
        HttpEntity<Object> person = getHttpEntity(
                "{\"name\": \"test 1\", \"email\": \"test10000000000001@gmail.com\","
                        + " \"registrationNumber\": \"41DCT\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
        ResponseEntity<Person> response = template.postForEntity(
                "/api/person", person, Person.class);
        //Delete this user
        personRepository.deleteById(response.getBody().getId());
        assertEquals("test 1", response.getBody().getName());
        assertEquals(200, response.getStatusCode().value());
    }

    private HttpEntity<Object> getHttpEntity(Object body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<Object>(body, headers);
    }

    @Test
    public void testFindAll() {
        ResponseEntity<List> response = template.getForEntity("/api/person/", List.class);
        assertNotEquals(0, response.getBody().size());
    }

    @Test
    public void testFindPersonById() {
        ResponseEntity<Person> response = template.getForEntity("/api/person/" + testId, Person.class);
        assertEquals("Jriz", response.getBody().getName());
        assertEquals("4DTC", response.getBody().getRegistrationNumber());
        assertEquals("jriz@gmail.com", response.getBody().getEmail());
    }
}
