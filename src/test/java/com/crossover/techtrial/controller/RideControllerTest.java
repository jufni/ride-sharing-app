package com.crossover.techtrial.controller;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.PersonRepository;
import com.crossover.techtrial.repositories.RideRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * @author Jufni R.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RideControllerTest {

    MockMvc mockMvc;

    @Autowired
    private RideController rideController;

    @Autowired
    private RideRepository rideRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TestRestTemplate template;

    private Long testRiderId;
    private Long testDriverId;
    private Long testRideId;
    private Long distance = 50l;
    private LocalDateTime testStartTime = LocalDateTime.of(2018, Month.JUNE, 8, 12, 12);
    private LocalDateTime testEndTime = LocalDateTime.of(2018, Month.JUNE, 8, 14, 12);

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(rideController).build();

        Person driver = new Person();
        driver.setName("Jane Doe");
        driver.setRegistrationNumber("5DTC");
        driver.setEmail("jdoe@gmail.com");
        personRepository.save(driver);
        testDriverId = driver.getId();

        Person rider = new Person();
        rider.setName("Jriz");
        rider.setRegistrationNumber("4DTC");
        rider.setEmail("jriz@gmail.com");
        personRepository.save(rider);
        testRiderId = rider.getId();

        Ride ride = new Ride();
        ride.setDriver(driver);
        ride.setRider(rider);
        ride.setDistance(distance);
        ride.setDriverId(testDriverId);
        ride.setRiderId(testRiderId);
        ride.setStartTime(testStartTime);
        ride.setEndTime(testEndTime);
        rideRepository.save(ride);
        testRideId = ride.getId();
    }

    @After
    public void clean() {
    	rideRepository.deleteById(testRideId);
    	personRepository.deleteById(testRiderId);
    	personRepository.deleteById(testDriverId);
    }
    
    @Test
    public void contextLoads() {
        assertThat(rideController).isNotNull();
    }

    @Test
    public void testRideIsRegistered() {
        HttpEntity<Object> rider = getHttpEntity(
                "{\"name\": \"rider 1\", \"email\": \"rider1@gmail.com\","
                        + " \"registrationNumber\": \"10001\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
        ResponseEntity<Person> riderResponse = template.postForEntity(
                "/api/person", rider, Person.class);

        HttpEntity<Object> driver = getHttpEntity(
                "{\"name\": \"driver 1\", \"email\": \"driver1@gmail.com\","
                        + " \"registrationNumber\": \"20001\",\"registrationDate\":\"2018-08-08T12:12:12\" }");
        ResponseEntity<Person> driverResponse = template.postForEntity(
                "/api/person", driver, Person.class);

        HttpEntity<Object> ride = getHttpEntity(
                "{\"driverId\": " + driverResponse.getBody().getId() +
                        ", \"riderId\": " + riderResponse.getBody().getId() +
                        ", \"distance\": 50" +
                        ", \"startTime\": \"2018-08-08T12:12:12\"" +
                        ", \"endTime\":\"2018-08-08T14:12:12\" }");
        ResponseEntity<Ride> rideResponse = template.postForEntity(
                "/api/ride", ride, Ride.class);

        rideRepository.deleteById(rideResponse.getBody().getId());
        assertEquals(driverResponse.getBody().getId(), rideResponse.getBody().getDriver().getId());
        assertEquals(riderResponse.getBody().getId(), rideResponse.getBody().getRider().getId());
        assertEquals(distance, rideResponse.getBody().getDistance());
        assertEquals(200, rideResponse.getStatusCode().value());
        
        // clean test
        personRepository.deleteById(driverResponse.getBody().getId());
        personRepository.deleteById(riderResponse.getBody().getId());
    }

    private HttpEntity<Object> getHttpEntity(Object body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<Object>(body, headers);
    }

    @Test
    public void testFindRideById() {
        ResponseEntity<Ride> response = template.getForEntity("/api/ride/" + testRideId, Ride.class);
        assertEquals(testDriverId, response.getBody().getDriver().getId());
        assertEquals(testRiderId, response.getBody().getRider().getId());
        assertEquals(distance, response.getBody().getDistance());
        assertEquals(testStartTime, response.getBody().getStartTime());
        assertEquals(testEndTime, response.getBody().getEndTime());
    }
}
