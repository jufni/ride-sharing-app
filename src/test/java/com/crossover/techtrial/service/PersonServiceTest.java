package com.crossover.techtrial.service;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.repositories.PersonRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import javax.validation.ConstraintViolationException;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    @Autowired
    @InjectMocks
    private PersonService personService = new PersonServiceImpl();

    @Test
    public void testFindByIdOK() {
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(new Person()));
        Person person = personService.findById(10000L);
        assertNotNull(person);
    }

    @Test
    public void testFindByIdNG() {
        when(personRepository.findById(10000L)).thenReturn(Optional.empty());
        assertNull(personService.findById(10000L));
    }

    @Test
    public void findAllOK() {
        List<Person> personList = Arrays.asList(new Person(), new Person());
        when(personRepository.findAll()).thenReturn(personList);
        assertEquals(2, personService.getAll().size());
    }

    @Test
    public void findAllNG() {
        when(personRepository.findAll()).thenReturn(Collections.emptyList());
        assertEquals(0, personService.getAll().size());
    }

    @Test
    public void testCreatePersonOK() {
        Person person = new Person();
        person.setName("Jriz");
        person.setRegistrationNumber("4DTC");
        person.setEmail("jriz@gmail.com");

        when(personRepository.save(person)).thenReturn(person);
        assertEquals(person, personService.save(person));
    }

    @Test(expected = ConstraintViolationException.class)
    public void testCreatePersonNG() {
        Person person = new Person();
        person.setName("Jriz");
        person.setRegistrationNumber("4DTC");
        
        when(personRepository.save(person)).thenThrow(new ConstraintViolationException(null));
        personService.save(person);
    }

}
