package com.crossover.techtrial.service;

import com.crossover.techtrial.dto.TopDriverDTO;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.PersonRepository;
import com.crossover.techtrial.repositories.RideRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolationException;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class RideServiceTest {
	
    @Mock
    private RideRepository rideRepository;
    
    @Mock
    private PersonRepository personRepository;
    
    @Autowired
    @InjectMocks
    private RideService rideService = new RideServiceImpl();
    
    private LocalDateTime testStartTime = LocalDateTime.of(2018, Month.JUNE, 8, 12, 12);
    private LocalDateTime testEndTime = LocalDateTime.of(2018, Month.JUNE, 8, 13, 12);
    
    @Test
    public void testFindByIdOK() {
        when(rideRepository.findById(anyLong())).thenReturn(Optional.of(new Ride()));
        Ride ride = rideService.findById(10000L);
        assertNotNull(ride);
    }

    @Test
    public void testFindByIdNG() {
        when(rideRepository.findById(10000L)).thenReturn(Optional.empty());
        assertNull(rideService.findById(10000L));
    }

    @Test
    public void testCreateRideOK() {
        Ride ride = new Ride();
        ride.setDriver(new Person());
        ride.setRider(new Person());
        ride.setDistance(50l);
        ride.setDriverId(101l);
        ride.setRiderId(201l);
        ride.setStartTime(testStartTime);
        ride.setEndTime(testEndTime);
        when(rideRepository.save(ride)).thenReturn(ride);
        assertEquals(ride, rideService.save(ride));
    }

    @Test(expected = ConstraintViolationException.class)
    public void testCreateRideNG() {
        Ride ride = new Ride();
        ride.setDriver(new Person());
        ride.setRider(new Person());
        ride.setDistance(50l);
        ride.setDriverId(101l);
        ride.setRiderId(201l);
        ride.setStartTime(testStartTime);
        ride.setEndTime(testEndTime);
        when(rideRepository.save(ride)).thenThrow(new ConstraintViolationException(null));
        rideService.save(ride);
    }
   
}
