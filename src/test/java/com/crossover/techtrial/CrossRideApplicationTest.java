/**
 *
 */
package com.crossover.techtrial;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.repositories.PersonRepository;
import com.crossover.techtrial.repositories.RideRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

/**
 * @author crossover
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CrossRideApplicationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void whenFindById_thenReturnPerson() {
        // given
        Person alex = new Person("Alex", "alex@example.com", "101");
        entityManager.persist(alex);
        entityManager.flush();

        System.out.println("alex ID " + alex.getId());
        // when
        Optional<Person> found = personRepository.findById(alex.getId());

        // then
        Assert.assertEquals(found.get().getName(), alex.getName());
        Assert.assertEquals(found.get().getEmail(), alex.getEmail());
        Assert.assertEquals(found.get().getRegistrationNumber(), alex.getRegistrationNumber());
    }

    @Test
    public void whenFindById_thenReturnRide() {
        // given
        Person alex = new Person("Alex", "alex@example.com", "101");
        entityManager.persist(alex);
        entityManager.flush();

        // when
        Optional<Person> found = personRepository.findById(alex.getId());

        // then
        Assert.assertEquals(found.get().getName(), alex.getName());
        Assert.assertEquals(found.get().getEmail(), alex.getEmail());
        Assert.assertEquals(found.get().getRegistrationNumber(), alex.getRegistrationNumber());
    }
}
