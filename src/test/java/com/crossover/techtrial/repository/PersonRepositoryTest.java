package com.crossover.techtrial.repository;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.repositories.PersonRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author Jufni R.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    private Long testId;

    @Before
    public void insertTestPerson() {
        Person person = new Person();
        person.setName("Jriz");
        person.setRegistrationNumber("4DTC");
        person.setEmail("jriz@gmail.com");
        person = personRepository.save(person);
        testId = person.getId();
    }
    
    @After
    public void clean() {
    	personRepository.deleteById(testId);
    }

    @Test
    public void testFindByIdOK() {
        Optional<Person> person = personRepository.findById(testId);
        assertTrue(person.isPresent());
        Person personObj = person.get();
        assertEquals("Jriz", personObj.getName());
        assertEquals("4DTC", personObj.getRegistrationNumber());
        assertEquals("jriz@gmail.com", personObj.getEmail());
    }

    @Test
    public void testFindByIdNG() {
        Optional<Person> person = personRepository.findById(10000000L);
        assertFalse(person.isPresent());
    }
}
