package com.crossover.techtrial.repository;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.PersonRepository;
import com.crossover.techtrial.repositories.RideRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author Jufni R.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class RideRepositoryTest {
	
    @Autowired
    private RideRepository rideRepository;
    
    @Autowired
    private PersonRepository personRepository;

    private Long testRiderId;
    private Long testDriverId;
    private Long testRideId;
    private Long distance = 50l;
    private LocalDateTime testStartTime = LocalDateTime.of(2018, Month.JUNE, 8, 12, 12);
    private LocalDateTime testEndTime = LocalDateTime.of(2018, Month.JUNE, 8, 14, 12);

    @Before
    public void insertTestRide() {
        Person driver = new Person();
        driver.setName("Jane Doe");
        driver.setRegistrationNumber("5DTC");
        driver.setEmail("jdoe@gmail.com");
        driver = personRepository.save(driver);
        testDriverId = driver.getId();

        Person rider = new Person();
        rider.setName("Jriz");
        rider.setRegistrationNumber("4DTC");
        rider.setEmail("jriz@gmail.com");
        rider = personRepository.save(rider);
        testRiderId = rider.getId();

        Ride ride = new Ride();
        ride.setDriver(driver);
        ride.setRider(rider);
        ride.setDistance(distance);
        ride.setDriverId(testDriverId);
        ride.setRiderId(testRiderId);
        ride.setStartTime(testStartTime);
        ride.setEndTime(testEndTime);
        ride = rideRepository.save(ride);
        testRideId = ride.getId();
    }
    
    @After
    public void removeTestRide() {    	
    	rideRepository.deleteById(testRideId);
    	personRepository.deleteById(testRiderId);
    	personRepository.deleteById(testDriverId);
    }

    @Test
    public void testFindByIdOK() {
        Optional<Ride> ride = rideRepository.findById(testRideId);
        assertTrue(ride.isPresent());
        Ride rideObj = ride.get();
        assertEquals(testDriverId, rideObj.getDriver().getId());
        assertEquals(testRiderId, rideObj.getRider().getId());
        assertEquals(distance, rideObj.getDistance());
        assertEquals(testStartTime, rideObj.getStartTime());
        assertEquals(testEndTime, rideObj.getEndTime());
    }

    @Test
    public void testFindByIdNG() {
        Optional<Ride> ride = rideRepository.findById(10000000L);
        assertFalse(ride.isPresent());
    }
}
