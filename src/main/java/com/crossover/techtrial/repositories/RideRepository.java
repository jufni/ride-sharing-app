/**
 * 
 */
package com.crossover.techtrial.repositories;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Ride repository for basic operations on Ride entity.
 * 
 * @author crossover
 */
@RestResource(exported = false)
public interface RideRepository extends CrudRepository<Ride, Long> {
	@Query("SELECT ride.driver, SUM(ride.distance) "
			+ "FROM Ride ride "
			+ "WHERE ride.startTime >= :startDate AND ride.endTime <= :endDate " 
			+ "GROUP BY ride.driverId "
			+ "ORDER BY SUM(ride.distance) DESC ")
	Optional<List> findTopDrivers(@Param("startDate") LocalDateTime startDate, 
			@Param("endDate") LocalDateTime endDate, Pageable pageable);

	Iterable<Ride> findAllByDriver(Person driver);
}
