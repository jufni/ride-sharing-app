/**
 *
 */
package com.crossover.techtrial.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;


/**
 * @author crossover
 */
@Entity
@Table(name = "person")
public class Person implements Serializable {

    private static final long serialVersionUID = 7401548380514451401L;

    public Person() {
    }

    public Person(String name, String email, String registrationNumber) {
        this.name = name;
        this.email = email;
        this.registrationNumber = registrationNumber;
    }
    
    public Person(Long id, String name, String email, String registrationNumber) {
    	this.id = id;
        this.name = name;
        this.email = email;
        this.registrationNumber = registrationNumber;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "name")
    String name;

    @NotNull
    @Email
    @Column(name = "email")
    String email;

    @Column(name = "registration_number")
    String registrationNumber;

    @JsonIgnore
    @OneToMany(mappedBy = "rider", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<Ride> riders = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "driver", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<Ride> drivers = new ArrayList<>();

    public List<Ride> getRiders() {
        return riders;
    }

    public void setRiders(List<Ride> riders) {
        this.riders = riders;
    }

    public void addRider(Ride rider) {
        if(riders != null) {
            riders.add(rider);
        }
    }

    public List<Ride> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Ride> drivers) {
        this.drivers = drivers;
    }

    public void addDriver(Ride driver) {
        if(drivers != null) {
            drivers.add(driver);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((registrationNumber == null) ? 0 : registrationNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (registrationNumber == null) {
            if (other.registrationNumber != null)
                return false;
        } else if (!registrationNumber.equals(other.registrationNumber))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Person [id=" + id + ", name=" + name + ", email=" + email + ", registrationNumber=" + registrationNumber + "]";
    }
}
