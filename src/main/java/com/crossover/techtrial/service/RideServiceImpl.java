/**
 *
 */
package com.crossover.techtrial.service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.StreamSupport;

import com.crossover.techtrial.dto.TopDriverDTO;
import com.crossover.techtrial.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.RideRepository;

/**
 * @author crossover
 */
@Service
public class RideServiceImpl implements RideService {

	@Autowired
	RideRepository rideRepository;

	public Ride save(Ride ride) {
		return rideRepository.save(ride);
	}

	public Ride findById(Long rideId) {
		Optional<Ride> optionalRide = rideRepository.findById(rideId);
		if (optionalRide.isPresent()) {
			return optionalRide.get();
		} else
			return null;
	}

    /**
     * This service returns the top 5 drivers (based on total distance traversed) with their 
     * 1) name, 2) email, 3) total minutes (totalRideDurationInMinutes), 
     * 4) maximum ride duration in minutes (maxRideDurationInMinutes),
     * 5) average distance per minute within the time range (averageDistance)
     * 
     * Condition:
     *   > Only rides that start and end within the mentioned durations are counted.
     *   > Any ride where either start or end time is outside the time parameters
     *   is not considered per requirement.
     */
	public List<TopDriverDTO> findTopDrivers(LocalDateTime startTime, LocalDateTime endTime, Long limit) {

		// Get data from db
		Optional<List> topDriverList = rideRepository.findTopDrivers(startTime, endTime, PageRequest.of(0, limit.intValue()));
		if (!topDriverList.isPresent()) {
			return null;
		}
		
		List<TopDriverDTO> topDrivers = new ArrayList<>();		
		List topList;
		try {
			topList = topDriverList.get();
		} catch (NoSuchElementException e) {
			return null;
		}

		for (int ctr = 0; ctr < topList.size(); ctr++) {
			Object[] result = (Object[]) topList.get(ctr);

			if (result[0] == null || result[1] == null) {
				return null;
			}

			Person driver = (Person) result[0];
			Long distance = (Long) result[1];
			
			Iterable<Ride> rides = rideRepository.findAllByDriver(driver);
			// get the total ride duration in minutes (totalRideDurationInMinutes)
			long total = StreamSupport.stream(rides.spliterator(), false)
					.mapToLong(ride -> ride.getStartTime().until(ride.getEndTime(), ChronoUnit.MINUTES)).sum();
			// get the maximum ride duration in minutes (maxRideDurationInMinutes)
			long max = StreamSupport.stream(rides.spliterator(), false)
					.mapToLong(ride -> ride.getStartTime().until(ride.getEndTime(), ChronoUnit.MINUTES)).max().getAsLong();
			// get the average distance per minute (averageDistance)
			double averageDistance = distance / total;
			
			topDrivers.add(new TopDriverDTO(driver.getName(), driver.getEmail(), total, max, averageDistance));
		}
		return topDrivers;
	}
}
