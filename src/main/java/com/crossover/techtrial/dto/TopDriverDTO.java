/**
 * 
 */
package com.crossover.techtrial.dto;

/**
 * @author crossover
 *
 */
public class TopDriverDTO {

	/**
	 * Constructor for TopDriverDTO
	 * 
	 * @param name
	 * @param email
	 * @param totalRideDurationInMinutes
	 * @param maxRideDurationInMinutes
	 * @param averageDistancePerMinute
	 */
	public TopDriverDTO(String name, String email, Long totalRideDurationInMinutes, Long maxRideDurationInMinutes,
			Double averageDistancePerMinute) {
		this.setName(name);
		this.setEmail(email);
		this.setTotalRideDurationInMinutes(totalRideDurationInMinutes);
		this.setMaxRideDurationInMinutes(maxRideDurationInMinutes);
		this.setAverageDistancePerMinute(averageDistancePerMinute);
	}

	public TopDriverDTO() {

	}

	private String name;

	private String email;

	private Long totalRideDurationInMinutes;

	private Long maxRideDurationInMinutes;

	private Double averageDistancePerMinute;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getTotalRideDurationInMinutes() {
		return totalRideDurationInMinutes;
	}

	public void setTotalRideDurationInMinutes(Long totalRideDurationInMinutes) {
		this.totalRideDurationInMinutes = totalRideDurationInMinutes;
	}

	public Long getMaxRideDurationInMinutes() {
		return maxRideDurationInMinutes;
	}

	public void setMaxRideDurationInMinutes(Long maxRideDurationInMinutes) {
		this.maxRideDurationInMinutes = maxRideDurationInMinutes;
	}

	public Double getAverageDistancePerMinute() {
		return averageDistancePerMinute;
	}

	public void setAverageDistancePerMinute(Double averageDistancePerMinute) {
		this.averageDistancePerMinute = averageDistancePerMinute;
	}

}
